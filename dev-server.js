const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const webpackConfig = require('./webpack.config');
const config = require("./config");

new WebpackDevServer(webpack(webpackConfig), {
    publicPath: webpackConfig.output.publicPath,
    hot: true,
    historyApiFallback: true,
    contentBase: 'public',
    inline: true,
    stats: {colors: true}
}).listen(config.dev.port, config.dev.host, function (err, result) {
    if (err) {
        return console.log(err)
    }
    console.log(result);
    console.log(`Dev server for MongoUI web app is listening at http://${config.dev.host}:${config.dev.port}/`)
});
