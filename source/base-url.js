let config = require('../config');

module.exports.API_URL = formatUrlString({
    protocol: process.env.API_PROTO || config.public.api.protocol,
    host: process.env.API_HOST || config.public.api.host,
    port: process.env.API_PORT || config.public.api.port,
});

function formatUrlString(uri) {
    return `${uri.protocol}://${uri.host}:${uri.port}`;
}
