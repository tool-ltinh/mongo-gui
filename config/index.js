const convict = require('convict');
const conf = convict(require("./default"));
module.exports = JSON.parse(conf.toString());
